<?php
namespace App\Model\Table;

use App\Model\Entity\Host;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Hosts Model
 *
 */
class HostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('hosts');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    public function findHostsOrderedByIP()
    {
        return $this->find()->order(['hostip']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('hostdesc', 'create')
            ->notEmpty('hostdesc', 'A Descrição do host é requerida!')
            ->add('hostdesc', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'O Nome/Descrição do host especificado já existe!'
                ]
            ]);

        $validator
            ->requirePresence('hostip', 'create')
            ->notEmpty('hostip', 'O IP do host é requerido!')
            ->add('hostip', 'custom', [
                'rule' => function($value, $ctx) {
                    return ($this->normalizeIp($value) != null);
                },
                'message' => 'O IP especificado é inválido!'
            ])
            ->add('hostip', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'O IP especificado já foi incluído!'
                ]
            ]);

        $validator
            ->add('fwdactive', 'valid', ['rule' => 'boolean'])
            ->requirePresence('fwdactive', 'create')
            ->notEmpty('fwdactive');

        return $validator;
    }

    public function normalizeIp($ip) {
        $ipSep = explode(".", $ip);
        $normIpArr = array();

        if (sizeof($ipSep) != 4) return null;

        foreach ($ipSep as $umIp) {
            if(is_numeric($umIp)) {
                $normIpArr[] = (int)$umIp;
            } else {
                return null;
            }
        }

        if($normIpArr[0] < 1 || $normIpArr[0] > 254) return null;
        if($normIpArr[1] < 0 || $normIpArr[1] > 255) return null;
        if($normIpArr[2] < 0 || $normIpArr[2] > 255) return null;
        if($normIpArr[3] < 3 || $normIpArr[3] > 254) return null;

        $normIp = implode($normIpArr, '.');

        return $normIp;
    }
}
