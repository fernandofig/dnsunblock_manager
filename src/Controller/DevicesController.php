<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class DevicesController extends AppController
{
	public function index()
	{
		$this->redirect(['action' => 'edit']);
	}

	public function getOrderedList($id = null)
	{
		if(!$this->request->is("ajax")) die("Request Invalido.");

		$hosts = $this->hostsQry->findHostsOrderedByIP();
		$this->set(compact('hosts'));

		if($id == 'forregionselect')
			$this->render('/Unblockconfig/get_region_device_list');
		else
			$this->render();
	}

	public function edit()
	{
		$host = $this->hostsQry->newEntity();
		$this->set(compact('host'));

		$this->render();
	}

	public function save()
	{
		if(!$this->request->isAll(["ajax","post"])) die("Request Invalido.");

		$normIp = $this->hostsQry->normalizeIp($this->request->data['hostip']);
		$normIp = $normIp == null ? $this->request->data['hostip'] : $normIp;

		$this->request->data['hostip'] = $normIp;
		$this->request->data['fwdactive'] = 0;

		$host = $this->hostsQry->newEntity($this->request->data);

		if ($this->hostsQry->save($host)) {
			$returnCode = 0;
			$returnErrors = [];
		} else {
			$erros = array();

			foreach($host->errors() as $sub)
				$erros = array_merge($erros, $sub);

			$erros = array_values($erros);
			array_walk($erros, function(&$item1) { $item1 = " - ".$item1; });

			$returnCode = 1;
			$returnErrors = $erros;
		}

		usleep(500000);

		$this->set(compact('returnCode', 'returnErrors'));
		$this->set('_serialize', ['returnCode', 'returnErrors']);
	}

	public function delete()
	{
		if(!$this->request->isAll(["ajax","post"])) die("Request Invalido.");

		$host = $this->hostsQry->get($this->request->data['hostid']);
		if ($this->hostsQry->delete($host)) {
			$returnCode = 0;
			$returnError = "";
		} else {
			$returnCode = 1;
			$returnError = "Não foi possível deletar o dispositivo (ID não existe?)";
		}

		usleep(500000);
		
		$this->set(compact('returnCode', 'returnError'));
		$this->set('_serialize', ['returnCode', 'returnError']);
	}
}