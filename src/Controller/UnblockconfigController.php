<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Routing\Router;
use Cake\View\AjaxView;
use Cake\View\Helper\UrlHelper;

class UnblockconfigController extends AppController
{
	public function index()
	{
		$this->redirect(['action' => 'change']);
	}

	public function change()
	{
		$this->render();
	}

	public function flipRegion($id) {
		$scriptDir = realpath(WWW_ROOT."../vendor/bin");

		$host = $this->hostsQry->get($id);
		$host->fwdactive = (int)$host->fwdactive === 0 ? 1 : 0;
		$this->hostsQry->save($host);

		$resultCode = '0';
		$resultDesc = 'OK';

		$newFlag = $host->fwdactive == 0 ? 'f-brazil.png' : 'f-us.png';
		$imgUrl = new UrlHelper(new AjaxView());
		$flagImg = $imgUrl->assetUrl($newFlag, ['pathPrefix' => Configure::read('App.imageBaseUrl')]);

		$activeFwdHosts = $this->hostsQry->findAllByFwdactive(1);
		$ips = array_map(function($i) { return $i->hostip; }, $activeFwdHosts->toArray());

		$ipArg = implode(" ", $ips);
		$cmd = "/usr/bin/sudo ".$scriptDir."/setForwardedIPs ".$ipArg;
		exec($cmd);

		$this->set(compact('resultCode', 'resultDesc', 'flagImg'));
		$this->set('_serialize', ['resultCode', 'resultDesc', 'flagImg']);

		usleep(500000);
	}
}
