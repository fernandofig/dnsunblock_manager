<table class="list devices">
	<?php
	foreach ($hosts as $host) {
		$cRegionFlag = $this->Html->image(
							($host->fwdactive ? 'f-us.png' : 'f-brazil.png'),
							[
								'id' => 'regFlag_'.$host->id,
								'alt' => ($host->fwdactive ? 'Estados Unidos' : 'Brasil'),
								'width' => '87',
								'height' => '50'
							]);
	?>
	<tr id="hostReg_<?= $host->id ?>" class="hostRow">
		<td><span title="<?= $host->hostip ?>"><?= $host->hostdesc ?></span></td>
		<td><?= $cRegionFlag . $this->element('loader', ['cssId' => 'ldHostReg_'.$host->id, 'hidden' => true]) ?></td>
	</tr>
	<?php
	}
	?>
</table>