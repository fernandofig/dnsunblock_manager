<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= $this->Html->meta('icon') ?>

    <title>.: Unblock Netflix : <?= $this->fetch('title') ?> :.</title>

    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('unblocknf.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Alternar navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="navbar-brand"><?= $this->Html->image('unblocknflogo.png', ['alt' => 'Unblock Netflix']) ?></span>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li <?= $this->name == 'Unblockconfig' ? ' class="active"' : '' ?>><?= $this->Html->link('Alterar Região', '/unblockconfig/change') ?></li>
                    <li <?= $this->name == 'Devices' ? ' class="active"' : '' ?>><?= $this->Html->link('Gerenciar Dispositivos', '/devices/edit') ?></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="unblocknf-content">
            <?= $this->Flash->render() ?>

            <div class="row">
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>

    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('nfunblock.js') ?>
    <?= $this->fetch('script') ?>
</body>
</html>
