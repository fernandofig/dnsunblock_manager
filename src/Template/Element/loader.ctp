<div <?= isset($cssId) && $cssId != '' ? "id=\"$cssId\"" : '' ?> class="loader" <?= isset($hidden) && $hidden === true ? 'style="display:none;"' : '' ?>>
  <div class="bounce1"></div>
  <div class="bounce2"></div>
  <div class="bounce3"></div>
</div>