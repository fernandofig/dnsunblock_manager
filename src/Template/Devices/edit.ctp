<?php
$this->assign('title', 'Adicionar / Remover dispositivo');
$this->Html->scriptBlock('$(function() { carregaListaDispositivos(); });', ['block' => true]);
?>
<div class="col-xs-12 col-sm-6">
	<?= $this->Form->create(null, [ 'action' => 'save', 'onsubmit' => 'return salvaHost();' ]) ?>
    <fieldset>
        <?php
            echo $this->Form->input('hostdesc', [ 'label' => 'Descrição da Estação: ', 'maxlength' => 96 ]);
            echo $this->Form->input('hostip', [ 'label' => 'End. IP da Estação: ', 'maxlength' => 15 ]);
            echo $this->Form->button(__('Incluir'));
        ?>
    </fieldset>
    <?= $this->Form->end() ?>
</div>
<div class="col-xs-12 col-sm-6">
	<hr />
	<?= $this->element('loader', ['cssId' => 'ldDispLista', 'hidden' => true]) ?>
	<div id="listaDisp" />
</div>