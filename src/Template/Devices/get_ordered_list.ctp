<table class="list deviceedit">
	<?php
	foreach ($hosts as $host) {
	?>
	<tr>
		<td><?= $host->hostdesc ?></td>
		<td class="hostipaddr"><?= $host->hostip ?></td>
		<td class="removebutton" id="disp_<?= $host->id ?>"><?= $this->Html->image('remove.png', [ 'alt' => 'Remover este Dispositivo', 'width' => '32', 'height' => '32' ]) ?></td>
	</tr>
	<?php
	}
	?>
</table>