# Unblock Manager

This is a small web application that makes it easier to swtich between DNS servers.

This is meant to be used with DNS services such as Unblock-US, Borderless Internet and others, that geographically unblocks content by proxying DNS queries.

Right now this is here only as a backup of my work. I don't intend to fully document it or make it portable right now, unless there's interest.

But in short, this requires:

1. A Linux box. This app should run on it;
2. A router (preferrably one that accepts a custom firmware, such as OpenWRT);
3. Special firewall rules on the Linux Box (not documented here ATM);
4. Special configurations on the router;
5. Maybe more stuff. Can't remember right now.
