function carregaListaRegiaoDispositivos() {
	$("#ldRegDispLista").show();
	$("div#listaRegiaoDisp")
		.html('')
		.load('/devices/getOrderedList/forregionselect', function() {
			$("#ldRegDispLista").hide();
			$(".hostRow").each(function() {
				var thisTr = $(this);
				var trCells = thisTr.children("td");
				trCells
					.on('mousedown touchstart', function() {
						if(!thisTr.hasClass('busyRow'))
							trCells.css('background-color', '#ccc');
					})
					.on('mouseup mouseleave touchend touchleave', function() {
						trCells.css('background-color', 'inherit');	
					})
					.click(function() {
						if(!thisTr.hasClass('busyRow')) {
							var hostId = thisTr[0].id.split('_')[1];
							mudaRegiao(hostId);
						}
					});
			});
		});
}

function mudaRegiao(id) {
	$("#regFlag_" + id).hide();
	$("#ldHostReg_" + id).show();
	$("#hostReg_" + id).addClass('busyRow');

	$.getJSON('/unblockconfig/flipRegion/' + id, function(json) {
		if(json.resultCode != 0) alert(json.resultDesc);

		$("#hostReg_" + id).removeClass('busyRow');
		$("#ldHostReg_" + id).hide();
		$("#regFlag_" + id)
			.attr('src', json.flagImg)
			.show();
	});
}

function carregaListaDispositivos() {
	$("#ldDispLista").show();
	$("div#listaDisp")
		.html('')
		.load('/devices/getOrderedList', function() {
			$("#ldDispLista").hide();
			$("table.deviceedit tr").each(function() {
				var thisTr = $(this);
				var trCells = thisTr.children("td.removebutton");
				trCells
					.on('mousedown touchstart', function() {
						trCells.css('background-color', '#ccc');
					})
					.on('mouseup mouseleave touchend touchleave', function() {
						trCells.css('background-color', 'inherit');	
					})
					.click(function() {
						var hostId = this.id.split('_')[1];
						deletaHost(hostId);
					});
			});
		});
}

function salvaHost() {
	var descHost = $("#hostdesc").val();
	var hostIp = $("#hostip").val();

	$("div#listaDisp").html('')
	$("#ldDispLista").show();

	formState(false);

	$.post("/devices/save.json", { "hostdesc" : descHost, "hostip" : hostIp }, function(jsonResp) {
		formState(true);
		if (jsonResp.returnCode != 0) {
			alert("Não foi possível salvar os dados do dispositivo devido ao(s) seguinte(s) erro(s):\n" + jsonResp.returnErrors.join("\n"));
			$("#hostdesc").focus();
		} else {
			$("#hostdesc").val("");
			$("#hostip").val("");
		}

		carregaListaDispositivos();
	}, "json");

	return false;
}

function formState(st) {
	$("fieldset").css('opacity', st ? '1.0' : '0.5');
	$("fieldset button, #hostdesc, #hostip").each(function() { this.disabled = !st; });
}

function deletaHost(id) {
	if(!isNaN(id) && confirm('Tem certeza que deseja remover este dispositivo?')) {
		$("#ldDispLista").show();
		$("div#listaDisp").html('');
		formState(false);

		$.post("/devices/delete.json", { "hostid": id }, function(jsonResp) {
			formState(true);
			if (jsonResp.returnCode != 0)
				alert("Não foi possível deletar o dispositivo devido ao seguinte erro:\n" + jsonResp.returnError);
			
			carregaListaDispositivos();
		}, "json");
	}
}